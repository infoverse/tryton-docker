SET default_transaction_read_only = off;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET default_tablespace = '';
SET default_with_oids = false;
SET statement_timeout = 0;
SET lock_timeout = 0;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;


create database tryton_test with encoding='UTF8' template='template0';
-- create database tryton_test with encoding='UTF8' LC_COLLATE='mk_MK.utf-8' LC_CTYPE='mk_MK.utf-8' template='template0';

create user tryton_test with password 'tryton_test';

grant all privileges on database tryton_test to tryton_test;





