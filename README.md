# Install

# Clone the project
 git clone git@gitlab.com:infoverse/tryton-docker.git
 
 copy .env-develop .env (edit if needed)
 docker-compose up

 if You see some errors, try:
 docker-compose up --remove-orphans 

## Install docker and docker-compose

    https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-18-04


## Shell acess
shell.sh

## First database setup
trytond-admin -c /etc/tryton/trytond.conf -d tryton_test --all

## Modules
/usr/local/lib/python3.6/dist-packages/trytond-5.6.9-py3.6.egg/trytond/modules

*** NOTE: *** See docker-compose.yaml for mapping modules folder


## Build image

    Go to the folder apache:  
    Update the `Dockerfile` with the changes.

    To build run:  

    `docker build -t infoversemk/apache .`

    **Test and verify image is working as espected.**

    To publish run:
    `docker push infoversemk/apache`
